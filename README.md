# course-service

Microservice for handling courses CRUD operation for IT-course web-site.

## Build

Build application
```bash
mvn clean install
```

Build application without running integration test
```bash
mvn clean install -DskipTests
```
**_NOTE:_** To `install` phase also included Checkstyle and Jacoco checks.

## Local Development

### Setup database
Run service `db` in [docker-compose.yml](docker-compose.yml) file

**_NOTE:_** If you won't use `db` from [docker-compose.yml](docker-compose.yml) - change default connection env var, while starting application.
- SPRING_DATASOURCE_URL
- SPRING_DATASOURCE_PASSWORD
- SPRING_DATASOURCE_USERNAME:postgres

### Run app via CLI
```bash
java -jar target/course-service-0.0.1-SNAPSHOT.jar
```

### Run app via IntelliJ Idea
```
Run app via Run/Debug adding the necessary variables in advance
```

## Deploy to Heroku
All deployment phases will automatically run via GitLab CI/CD see [.gitlab-ci.yml](.gitlab-ci.yml) file

## Code Owners
|     Full name     |   Role    |          Email          |
|:-----------------:|:---------:|:-----------------------:|
| Artemii Aleksenko | Developer | aleks.artem24@gmail.com |
