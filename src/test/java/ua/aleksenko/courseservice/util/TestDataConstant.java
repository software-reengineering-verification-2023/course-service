package ua.aleksenko.courseservice.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TestDataConstant {

	public static final String TEST_USER_EMAIL = "artemii@gmail.com";
}
