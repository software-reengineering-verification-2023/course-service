package ua.aleksenko.courseservice.util;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import ua.aleksenko.courseservice.AbstractIntegrationTest;
import ua.aleksenko.courseservice.model.dto.CreateCourseDto;

@Component
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ApiUtil extends AbstractIntegrationTest {

	private final MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	public ApiUtil(MockMvc mockMvc) {
		this.mockMvc = mockMvc;
	}

	@SneakyThrows
	public UUID createDefaultCourse() {
		CreateCourseDto newCourse = getDefaultCourse();

		String id = mockMvc.perform(post("/api/v1/course/create")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(newCourse)))
				.andExpect(status().isCreated())
				.andReturn()
				.getResponse()
				.getContentAsString();

		return UUID.fromString(id);
	}

	public CreateCourseDto getDefaultCourse() {
		return new CreateCourseDto("Java for Beginner", "desc", "some url",
				"English", "Backend", "Artemii Aleksenko", List.of("url/lesson1.aws", "url/lesson2.aws"));
	}
}
