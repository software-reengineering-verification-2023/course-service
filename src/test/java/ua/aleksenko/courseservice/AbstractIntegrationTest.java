package ua.aleksenko.courseservice;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;

import lombok.extern.slf4j.Slf4j;

@ActiveProfiles("test")
@SpringBootTest(classes = CourseServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Slf4j
public abstract class AbstractIntegrationTest {

	private static final String JDBC_URL_FORMAT = "jdbc:postgresql://%s:%s/%s";
	private static final String DOCKER_IMAGE_NAME = "postgres:15";

	static final PostgreSQLContainer<?> POSTGRESQL_CONTAINER =
			new PostgreSQLContainer<>(DOCKER_IMAGE_NAME);

	@DynamicPropertySource
	static void postgresProperties(DynamicPropertyRegistry registry) {
		POSTGRESQL_CONTAINER.start();
		log.info("Test container started successfully! \n "
				+ "JDBC URL: " + getJdbcUrl() + " \n"
				+ "DB_USERNAME: " + POSTGRESQL_CONTAINER.getUsername() + " \n"
				+ "DB_PASSWORD: " + POSTGRESQL_CONTAINER.getUsername());
		registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl);
		registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername);
		registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword);
	}

	private static String getJdbcUrl() {
		return String.format(JDBC_URL_FORMAT,
				POSTGRESQL_CONTAINER.getHost(),
				POSTGRESQL_CONTAINER.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT),
				POSTGRESQL_CONTAINER.getDatabaseName());
	}
}
