package ua.aleksenko.courseservice.controller;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Path;
import lombok.extern.slf4j.Slf4j;
import ua.aleksenko.courseservice.model.error.ApiError;
import ua.aleksenko.courseservice.model.error.ValidationError;
import ua.aleksenko.courseservice.model.exception.CourseNotFoundException;

@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    private static final String CONSTRAINT_VIOLATION_MESSAGE = "Validation failed";

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ValidationError handleConstraintViolationException(ConstraintViolationException e) {
        Map<String, String> errors = e.getConstraintViolations().stream().collect(
                Collectors.toMap(constraintViolation ->
                                StreamSupport.stream(constraintViolation.getPropertyPath().spliterator(), false)
                                        .reduce((first, second) -> second)
                                        .map(Path.Node::getName).orElse(null),
                        ConstraintViolation::getMessage));
        log.error("ConstraintViolationException handled due to errors: {}", errors);
        return new ValidationError(HttpStatus.BAD_REQUEST.value(), CONSTRAINT_VIOLATION_MESSAGE, errors);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ValidationError handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        Map<String, String> errors = e.getFieldErrors().stream()
                .collect(Collectors.toMap(
                        FieldError::getField,
                        o -> Optional.ofNullable(o.getDefaultMessage()).orElse("Message is unavailable")
                ));
        log.error("MethodArgumentNotValidException handled due to errors: {}", errors);
        return new ValidationError(HttpStatus.BAD_REQUEST.value(), CONSTRAINT_VIOLATION_MESSAGE, errors);
    }

    @ExceptionHandler(CourseNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleCourseNotFoundException(CourseNotFoundException e) {
        log.error("CourseNotFoundException handled with error: {}", e.getMessage());
        return new ApiError(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ApiError handleAccessDeniedException(AccessDeniedException e) {
		log.error("AccessDeniedException handled with error: ", e);
		return new ApiError(HttpStatus.FORBIDDEN.value(), e.getMessage());
	}

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleAllOtherException(RuntimeException e) {
        log.error("RuntimeException handled with error: ", e);
        return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }
}
