package ua.aleksenko.courseservice.model.exception;

import java.util.UUID;

public class CourseNotFoundException extends RuntimeException {

	private static final String ERROR_MESSAGE = "Course with id = %s does not exist!";

	public CourseNotFoundException(UUID matrixId) {
		super(String.format(ERROR_MESSAGE, matrixId));
	}
}
