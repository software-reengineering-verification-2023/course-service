package ua.aleksenko.courseservice.model.dto;

import java.util.UUID;

public record NewestCourseDto(
		UUID id,
		String title,
		String imageUrl,
		String source,
		Integer lessonsAmount,
		String language) {
}
