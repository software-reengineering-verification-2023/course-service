package ua.aleksenko.courseservice.model.dto;

import java.util.UUID;

public record LessonDto(
		UUID id,
		String title,
		String videoUrl,
		String newTitle) {
}
