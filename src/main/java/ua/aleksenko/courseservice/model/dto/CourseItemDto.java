package ua.aleksenko.courseservice.model.dto;

import java.util.UUID;

public record CourseItemDto(
		UUID id,
		String title,
		String description,
		String imageUrl) {
}
