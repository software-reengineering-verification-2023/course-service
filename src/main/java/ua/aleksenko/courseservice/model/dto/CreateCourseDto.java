package ua.aleksenko.courseservice.model.dto;

import java.util.List;

public record CreateCourseDto(String title,
                              String description,
                              String imageUrl,
                              String language,
                              String category,
                              String source,
                              List<String> videoUrls) {
}
