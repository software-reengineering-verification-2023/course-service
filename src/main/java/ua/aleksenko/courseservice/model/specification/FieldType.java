package ua.aleksenko.courseservice.model.specification;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import lombok.extern.slf4j.Slf4j;
import ua.aleksenko.courseservice.model.entity.Category;
import ua.aleksenko.courseservice.model.entity.Language;

@Slf4j
public enum FieldType {

	BOOLEAN {
		public Object parse(String value) {
			return Boolean.valueOf(value);
		}
	},

	CHAR {
		public Object parse(String value) {
			return value.charAt(0);
		}
	},

	DATE {
		public Object parse(String value) {
			Object date = null;
			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
				date = LocalDateTime.parse(value, formatter);
			} catch (Exception e) {
				log.info("Failed parse field type DATE {}", e.getMessage());
			}

			return date;
		}
	},

	DOUBLE {
		public Object parse(String value) {
			return Double.valueOf(value);
		}
	},

	INTEGER {
		public Object parse(String value) {
			return Integer.valueOf(value);
		}
	},

	LONG {
		public Object parse(String value) {
			return Long.valueOf(value);
		}
	},

	STRING {
		public Object parse(String value) {
			return value;
		}
	},

	CATEGORY {
		public Object parse(String value) {
			return Category.findValueByName(value);
		}
	},

	LANGUAGE {
		public Object parse(String value) {
			return Language.findValueByName(value);
		}
	};

	public abstract Object parse(String value);

}
