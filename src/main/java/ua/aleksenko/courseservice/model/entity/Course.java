package ua.aleksenko.courseservice.model.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.hypersistence.utils.hibernate.type.basic.PostgreSQLEnumType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "course")
public class Course {

	@Id
	@GeneratedValue
	private UUID id;

	private String title;

	private String description;

	private String imageUrl;

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "language_type")
	@Type(PostgreSQLEnumType.class)
	private Language language;

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "category_type")
	@Type(PostgreSQLEnumType.class)
	private Category category;

	@CreatedDate
	@Column(updatable = false, nullable = false)
	private LocalDate createdAt;

	@LastModifiedDate
	@Column(nullable = false)
	private LocalDate updatedAt;

	@ManyToOne
	@JoinColumn(name = "source_id", nullable = false)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private Source source;

	@OneToMany(mappedBy = "course", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private List<Lesson> lessons = new ArrayList<>();

	public Course(String title, String description, String imageUrl, Language language, Category category, Source source) {
		this.title = title;
		this.description = description;
		this.imageUrl = imageUrl;
		this.language = language;
		this.category = category;
		this.source = source;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
			return false;
		}
		Course course = (Course) o;
		return id != null && Objects.equals(id, course.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}
