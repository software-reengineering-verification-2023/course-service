package ua.aleksenko.courseservice.model.entity;

import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Category {
	FRONTEND("Frontend"),
	BACKEND("Backend"),
	DEV_OPS("DevOps"),
	MARKETING("Marketing"),
	GRAPHIC("Graphic"),
	VIDEO("Video"),
	GAME_DEV("GameDev"),
	TESTING("Testing"),
	FINANCE("Finance");

	private final String name;

	public static Category findValueByName(String name) {
		return Arrays.stream(Category.values())
				.filter(category -> category.getName().equals(name)).findFirst()
				.orElseThrow(() -> new RuntimeException(String.format("Unknown Category.name: '%s'", name)));
	}

	public static List<String> getAllCategories() {
		return Arrays.stream(Category.values()).map(Category::getName).toList();
	}
}
