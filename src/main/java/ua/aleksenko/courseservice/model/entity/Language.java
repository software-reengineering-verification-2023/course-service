package ua.aleksenko.courseservice.model.entity;

import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Language {
	ENGLISH("English"),
	UKRAINIAN("Ukrainian");

	private final String name;

	public static Language findValueByName(String name) {
		return Arrays.stream(Language.values())
				.filter(category -> category.getName().equals(name)).findFirst()
				.orElseThrow(() -> new RuntimeException(String.format("Unknown Language.name: '%s'", name)));
	}

	public static List<String> getAllLanguages() {
		return Arrays.stream(Language.values()).map(Language::getName).toList();
	}
}
