package ua.aleksenko.courseservice.model.entity;

import java.util.Objects;
import java.util.UUID;

import org.hibernate.Hibernate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "lesson")
public class Lesson {

	@Id
	@GeneratedValue
	private UUID id;

	private String title;

	private String videoUrl;

	@ManyToOne
	@JoinColumn(name = "course_id", nullable = false)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private Course course;

	public Lesson(String title, String videoUrl, Course course) {
		this.title = title;
		this.videoUrl = videoUrl;
		this.course = course;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
			return false;
		}
		Lesson lesson = (Lesson) o;
		return id != null && Objects.equals(id, lesson.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}
