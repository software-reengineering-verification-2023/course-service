package ua.aleksenko.courseservice.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.aleksenko.courseservice.model.entity.Lesson;

public interface LessonRepository extends JpaRepository<Lesson, UUID> {

}
