package ua.aleksenko.courseservice.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.aleksenko.courseservice.model.entity.Source;

public interface SourceRepository extends JpaRepository<Source, UUID> {

	Optional<Source> findByNameIgnoreCase(String name);
}
