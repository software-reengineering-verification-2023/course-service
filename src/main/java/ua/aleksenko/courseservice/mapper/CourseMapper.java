package ua.aleksenko.courseservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import ua.aleksenko.courseservice.model.dto.NewestCourseDto;
import ua.aleksenko.courseservice.model.dto.CourseDetailsDto;
import ua.aleksenko.courseservice.model.dto.CourseItemDto;
import ua.aleksenko.courseservice.model.entity.Course;

@Mapper(componentModel = "spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CourseMapper {

	CourseItemDto toDto(Course course);

	List<CourseItemDto> toCourseItemDtos(List<Course> courses);

	@Mapping(target = "language", source = "language.name")
	@Mapping(target = "category", source = "category.name")
	@Mapping(target = "source", source = "source.name")
	CourseDetailsDto toCourseDetailsDto(Course course);

	List<NewestCourseDto> toNewestCourseDtos(List<Course> newestCourse);

	@Mapping(target = "language", source = "language.name")
	@Mapping(target = "source", source = "source.name")
	@Mapping(target = "lessonsAmount",
			expression = """
					java(
					    newestCourse.getLessons().size()
					)
					"""
	)
	NewestCourseDto toNewestCourseDto(Course newestCourse);
}
