package ua.aleksenko.courseservice.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ua.aleksenko.courseservice.mapper.CourseMapper;
import ua.aleksenko.courseservice.model.dto.CourseDetailsDto;
import ua.aleksenko.courseservice.model.dto.CourseItemDto;
import ua.aleksenko.courseservice.model.dto.CreateCourseDto;
import ua.aleksenko.courseservice.model.dto.PageResponse;
import ua.aleksenko.courseservice.model.dto.UpdateCourseDto;
import ua.aleksenko.courseservice.model.entity.Category;
import ua.aleksenko.courseservice.model.entity.Course;
import ua.aleksenko.courseservice.model.entity.Language;
import ua.aleksenko.courseservice.model.entity.Lesson;
import ua.aleksenko.courseservice.model.entity.Source;
import ua.aleksenko.courseservice.model.exception.CourseNotFoundException;
import ua.aleksenko.courseservice.model.specification.SearchRequest;
import ua.aleksenko.courseservice.model.specification.SearchSpecification;
import ua.aleksenko.courseservice.repository.CourseRepository;
import ua.aleksenko.courseservice.repository.LessonRepository;
import ua.aleksenko.courseservice.repository.SourceRepository;
import ua.aleksenko.courseservice.service.CourseService;

@Service
@RequiredArgsConstructor
@Slf4j
public class CourseServiceImpl implements CourseService {

	private final CourseRepository courseRepository;
	private final SourceRepository sourceRepository;
	private final LessonRepository lessonRepository;
	private final CourseMapper mapper;

	@Override
	@Transactional(readOnly = true)
	public PageResponse<CourseItemDto> searchCourses(SearchRequest request) {
		log.info("Start executing method searchCourses: {}", request);
		request.setPage(request.getPage() - 1);
		SearchSpecification<Course> specification = new SearchSpecification<>(request);
		Pageable pageable = SearchSpecification.getPageable(request.getPage(), request.getSize());
		Page<Course> coursesPage = courseRepository.findAll(specification, pageable);
		List<CourseItemDto> employeesShortInfoDto = mapper.toCourseItemDtos(coursesPage.getContent());
		PageResponse<CourseItemDto> pageResponse = new PageResponse<>(employeesShortInfoDto, coursesPage);
		pageResponse.setPageNumber(pageResponse.getPageNumber() + 1);
		log.info("Finish executing method searchCourses with result: {}", pageResponse);
		return pageResponse;
	}

	@Override
	@Transactional(readOnly = true)
	public CourseDetailsDto getCourseDetails(UUID id) {
		log.info("Start executing method getCourseDetails for course: {}", id);
		Course course = courseRepository.findById(id)
				.orElseThrow(() -> new CourseNotFoundException(id));
		CourseDetailsDto courseDetailsDto = mapper.toCourseDetailsDto(course);
		log.info("Finish executing method getCourseDetails with result: {}", courseDetailsDto);
		return courseDetailsDto;
	}

	@Override
	public List<String> getAllCategories() {
		log.info("Start executing method getAllCategories");
		List<String> allCategories = Category.getAllCategories();
		log.info("Finish executing method getAllCategories with result: {}", allCategories);
		return allCategories;
	}

	@Override
	public List<String> getAllLanguages() {
		log.info("Start executing method getAllLanguages.");
		List<String> allLanguages = Language.getAllLanguages();
		log.info("Finish executing method getAllLanguages with result: {}", allLanguages);
		return allLanguages;
	}

	@Override
	@Transactional
	public void deleteCourse(UUID id) {
		log.info("Start executing method deleteCourse for course: {}", id);
		Course course = courseRepository.findById(id)
				.orElseThrow(() -> new CourseNotFoundException(id));
		courseRepository.delete(course);
		log.info("Finish executing method deleteCourse.");
	}

	@Override
	@Transactional
	public UUID create(CreateCourseDto dto) {
		log.info("Start adding new course: {}", dto);
		Source source = sourceRepository.findByNameIgnoreCase(dto.source())
				.orElseGet(() -> sourceRepository.save(new Source(dto.source())));

		Course course = courseRepository.save(new Course(dto.title(),
				dto.description(),
				dto.imageUrl(),
				Language.findValueByName(dto.language()),
				Category.findValueByName(dto.category()),
				source));

		List<Lesson> lessons = lessonRepository.saveAll(dto.videoUrls().stream()
				.map(videoUrl -> new Lesson(getLessonTitle(videoUrl), videoUrl, course))
				.toList());

		return course.getId();
	}

	@Override
	@Transactional
	public void update(UpdateCourseDto dto) {
		log.info("Start updating course: {}", dto.title());
		Source source = sourceRepository.findByNameIgnoreCase(dto.source())
				.orElseGet(() -> sourceRepository.save(new Source(dto.source())));
		Course course = courseRepository.findById(dto.id())
				.orElseThrow(() -> new CourseNotFoundException(dto.id()));
		course.setTitle(dto.title());
		course.setDescription(dto.description());
		course.setSource(source);
		course.setLanguage(Language.findValueByName(dto.language()));
		course.setCategory(Category.findValueByName(dto.category()));
		course.setImageUrl(dto.imageUrl());

		courseRepository.saveAndFlush(course);

		dto.lessonsToDelete().removeIf(Objects::isNull);
		lessonRepository.deleteAllById(dto.lessonsToDelete());

		dto.editedLessons().forEach(editedLesson -> {
					Optional<Lesson> lessonOptional = lessonRepository.findById(editedLesson.id());
					if (lessonOptional.isPresent() && Objects.nonNull(editedLesson.newTitle())) {
						Lesson lesson = lessonOptional.get();
						lesson.setTitle(editedLesson.newTitle());
						lessonRepository.saveAndFlush(lesson);
					}
				}
		);
		if (Objects.nonNull(dto.newLessons())) {
			lessonRepository.saveAll(dto.newLessons().stream()
					.map(videoUrl -> new Lesson(getLessonTitle(videoUrl), videoUrl, course))
					.toList());
		}
	}

	private String getLessonTitle(String videoUrl) {
		return videoUrl.substring(videoUrl.lastIndexOf('/') + 1, videoUrl.lastIndexOf('.'));
	}


}
