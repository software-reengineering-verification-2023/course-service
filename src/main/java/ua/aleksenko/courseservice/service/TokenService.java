package ua.aleksenko.courseservice.service;

public interface TokenService {

    void validateExpirationDate(String token);

    void validateToken(String token);
}
