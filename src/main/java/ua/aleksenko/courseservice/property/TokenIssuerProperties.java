package ua.aleksenko.courseservice.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "token-issuer")
public class TokenIssuerProperties {

    private String baseUrl;
    private String tokenValidationEndpoint;
}
