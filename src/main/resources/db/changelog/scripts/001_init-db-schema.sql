-- liquibase formatted sql
-- changeset artemii.aleksenko:001_init-db-schema.sql
CREATE TYPE language_type AS ENUM ('ENGLISH', 'UKRAINIAN');
-- rollback DROP TYPE language_type;

CREATE TYPE category_type AS ENUM ('FRONTEND', 'BACKEND', 'DEV_OPS', 'MARKETING', 'GRAPHIC', 'VIDEO', 'GAME_DEV', 'TESTING', 'FINANCE');
-- rollback DROP TYPE language_type;

CREATE CAST (character varying AS language_type) WITH INOUT AS IMPLICIT;
CREATE CAST (character varying AS category_type) WITH INOUT AS IMPLICIT;

CREATE TABLE "source"
(
    "id"          UUID PRIMARY KEY,
    "name"        VARCHAR(255) UNIQUE NOT NULL
);
-- rollback DROP TABLE source;

CREATE TABLE "course"
(
    "id"             UUID PRIMARY KEY,
    "title"          VARCHAR(255)  NOT NULL,
    "description"    VARCHAR(1000) NOT NULL,
    "image_url"      VARCHAR(1000) NOT NULL,
    "language"       language_type NOT NULL,
    "category"       category_type NOT NULL,
    "created_at"     date     NOT NULL,
    "updated_at"     date     NOT NULL,
    "source_id"      UUID          NOT NULL,
    CONSTRAINT "FK_course.source_id" FOREIGN KEY ("source_id") REFERENCES "source" ("id") ON DELETE CASCADE
);
-- rollback DROP TABLE course;

CREATE TABLE "lesson"
(
    "id"        UUID PRIMARY KEY,
    "title"     VARCHAR(255)  NOT NULL,
    "video_url" VARCHAR(1000) NOT NULL,
    "course_id" UUID          NOT NULL,
    CONSTRAINT "FK_lesson.course_id" FOREIGN KEY ("course_id") REFERENCES "course" ("id")
);
-- rollback DROP TABLE lesson;
